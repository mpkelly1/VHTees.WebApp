import { Component } from '@angular/core';
import { Router, NavigationStart } from '@angular/router';
import 'rxjs/add/operator/filter';

@Component({
   selector: 'header',
   templateUrl: './header.component.html',
   styleUrls: ['./header.component.css']
})
export class HeaderComponent {

	private router: Router;
	private currentRoute: string;

	constructor(router: Router){
		this.router = router;

		this.router.events
           .filter(event => event instanceof NavigationStart)
           .subscribe((event: NavigationStart) => {
               if (event.url.indexOf('items') > -1) {
                   this.currentRoute = 'items';
               } else if (event.url.indexOf('ContactUs') > -1) {
                   this.currentRoute = 'ContactUs';
               } else {
                   this.currentRoute = 'items';
               }
           });

	}

	goToRoute(url: string){
		this.router.navigate([url]);
	}
}
