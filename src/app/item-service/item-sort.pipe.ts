import { Pipe, PipeTransform } from '@angular/core';

import { Item } from './item';

@Pipe({name: 'itemSort'})
export class ItemSortPipe implements PipeTransform {
    transform(values: Item[], order?: string) {
        if (values) {
            values.sort((first: Item, second: Item) => {
                if (first.price < second.price) {
                    return order === 'asc' ? 1 : -1;
                } else if (first.price > second.price) {
                    return order === 'asc' ? -1 : 1;
                } else {
                    return 0;
                }
            });
        }
        return values;
    }
}
