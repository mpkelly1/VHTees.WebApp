export class Item {
    inventoryId: number;
    title: string;
    size: string;
    color: string;
    price: number;
    inventoryRemaining: number;
    productImg: string;
}
