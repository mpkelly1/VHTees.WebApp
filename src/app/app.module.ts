import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { ItemListComponent } from './item-list/item-list.component';
import { ItemDetailsComponent } from './item-details/item-details.component';
import { ItemCardComponent } from './item-card/item-card.component';
import { ItemService } from './item-service/item.service';
import { ItemSortPipe } from './item-service/item-sort.pipe';
import { ContactUsComponent } from './contact-us/contact-us.component';

@NgModule({
   declarations: [
       AppComponent,
       HeaderComponent,
       ItemListComponent,
       ItemDetailsComponent,
       ItemCardComponent,
       ItemSortPipe,
       ContactUsComponent
    ],
   imports: [
       BrowserModule, 
       RouterModule.forRoot([
           {
               path: '',
               redirectTo: 'projects',
               pathMatch: 'full'
           },
           {
               path: 'projects',
               component: ItemListComponent
           },
            {
               path: 'projects/:inventoryId',
               component: ItemDetailsComponent
           },
           {
             path: 'ContactUs',
             component: ContactUsComponent
           }
       ]),
       HttpModule,
       FormsModule
    ],
   providers: [ItemService],
   bootstrap: [AppComponent]
})
export class AppModule { }
